package A3Bloc1;

/*IES Sabadell.
CFGS DAM M03 UF1
Bloc 1 Exercici 9
Descripcio: Algorisme que llegeix un n�mero i ens diu si �s m�ltiple de dos o de tres o de cap d�ells.
Autor: David L�pez 
 */

import java.util.Scanner;

public class Exercici9 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner reader = new Scanner(System.in);
		int num1 = 0;
		
		System.out.print("Intodueix un numero per saber si es multiple de 2 o de 3, o si no �s multiple de cap dels dos: ");
		num1 = reader.nextInt();
		
		String mult2 = "El numero "+num1+" �s multiple de 2";
		String mult3 = "El numero "+num1+" �s multiple de 3";
		String res = "El numero "+num1+" no �s multiple de 2 ni multiple de 3";
		reader.close();
		
		if (num1%2 == 0) {
			System.out.print(mult2);
		}
		else {
			if (num1%3 == 0) {
				System.out.print(mult3);
			}
			else {
				System.out.print(res);
			}
		}

	}

}
